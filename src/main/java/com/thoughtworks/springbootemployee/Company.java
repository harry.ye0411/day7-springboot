package com.thoughtworks.springbootemployee;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
@Getter
@Setter
public class Company {
    private Long companyId;
    private String name;


    public Company() {
    }

    public Company(long companyId, String name) {
        this.companyId = companyId;
        this.name = name;
    }

    public Long getCompanyId() {
        return companyId;
    }

    public String getName() {
        return name;
    }
}
