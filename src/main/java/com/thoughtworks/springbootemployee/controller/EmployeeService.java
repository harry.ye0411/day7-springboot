package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.Employee;
import com.thoughtworks.springbootemployee.repository.EmployeeRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getEmployeesByCompanyID(Long companyID) {
        return employeeRepository.findEmployeesByCompanyId(companyID);
    }

    public void addEmployeesWithCompany(List<Employee> employees, Long companyID) {
        employees.stream().forEach(employee -> {
            employee.setCompanyId(companyID);
            employeeRepository.addEmployee(employee);
        });
    }

}