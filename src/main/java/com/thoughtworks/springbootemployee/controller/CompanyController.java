package com.thoughtworks.springbootemployee.controller;

import com.thoughtworks.springbootemployee.AddCompanyRequest;
import com.thoughtworks.springbootemployee.Company;
import com.thoughtworks.springbootemployee.Employee;
import com.thoughtworks.springbootemployee.repository.CompanyRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/companies")
public class CompanyController {
    private final CompanyRepository companyRepository;
    private final EmployeeService employeeService;

    public CompanyController(CompanyRepository companyRepository, EmployeeService employeeService) {
        this.companyRepository = companyRepository;
        this.employeeService = employeeService;
    }

    @GetMapping
    public List<Company> getAllCompanies(){
        return companyRepository.findAll();
    }
    @GetMapping(params = "/{companyId}")
    public Company getCompanyById(String companyId){
        return companyRepository.findById(Long.parseLong(companyId));
    }

    @GetMapping(path = "/{companyID}/employees")
    public List<Employee> getEmployeesByCompanyID(@PathVariable Long companyID) {
        return employeeService.getEmployeesByCompanyID(companyID);
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Company addCompany(@RequestBody AddCompanyRequest addCompanyRequest) {
        Company newCompany = companyRepository.addCompany(addCompanyRequest.getName());
        employeeService.addEmployeesWithCompany(addCompanyRequest.getEmployees(), newCompany.getCompanyId());
        return newCompany;
    }

    @PutMapping(path = "/{id}")
    public Company updateCompany(@PathVariable Long id, @RequestBody Company company) {
        return companyRepository.updateCompany(id, company);
    }

    @DeleteMapping(path = "/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Long id) {
        companyRepository.deleteCompany(id);
    }

}
