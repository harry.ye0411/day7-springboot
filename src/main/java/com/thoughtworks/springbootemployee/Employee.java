package com.thoughtworks.springbootemployee;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@AllArgsConstructor
@Getter
public class Employee implements Serializable {

    @Setter
    private Long id;
    private String name;
    @Setter
    private Integer age;
    private String gender;
    @Setter
    private Integer salary;
    private Long companyId;

    public Long getCompanyId() {
        return companyId;
    }

    public void setCompanyId(Long companyId) {
        this.companyId = companyId;
    }
}
