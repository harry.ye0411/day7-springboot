package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.Company;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class CompanyRepository {
    List<Company> companies;

    public CompanyRepository(List<Company> companies) {
        this.companies = companies;
        companies.add(new Company(1L,"OOCL"));
        companies.add(new Company(2L,"COSCON"));
    }

    public List<Company> findAll() {
        return companies;
    }

    public Company findById(Long id) {
        return companies.stream()
                .filter(company -> company.getCompanyId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public Company addCompany(String name) {
        Long id = generateID();
        Company newCompany = new Company(id, name);
        companies.add(newCompany);
        return newCompany;
    }

    private Long generateID() {
        return companies.stream()
                .mapToLong(Company::getCompanyId)
                .max()
                .orElse(0L) + 1;
    }

    public Company updateCompany(Long id, Company company) {
        Company targetCompany = findById(id);
        targetCompany.setName(company.getName());
        return  targetCompany;
    }

    public void deleteCompany(Long id) {
        Company targetCompany = findById(id);
        companies.remove(targetCompany);
    }

}
