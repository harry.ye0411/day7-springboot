package com.thoughtworks.springbootemployee.repository;

import com.thoughtworks.springbootemployee.Employee;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Repository
public class EmployeeRepository {

    private List<Employee> employees;

    public EmployeeRepository() {
        this.employees = new ArrayList<>();
        employees.add(new Employee(1L, "Harry", 99, "male", 9999, 1L));
        employees.add(new Employee(2L, "Harry2", 99, "male", 22222, 2L));
    }

    public List<Employee> findAll() {
        return employees;
    }

    public Employee findById(Long id) {
        return employees.stream()
                .filter(employee -> employee.getId().equals(id))
                .findFirst()
                .orElse(null);
    }

    public List<Employee> getByGender(String gender) {
        return employees.stream()
                .filter(employee -> employee.getGender().equals(gender))
                .toList();
    }

    public Employee addEmployee(Employee employee) {
        Long id = generateId();
        employee.setId(id);
        employees.add(employee);
        return employee;
    }

    private long generateId() {
        return employees.stream()
                .mapToLong(Employee::getId)
                .max()
                .orElse(0L) + 1L;
    }

    public Employee updateEmployee(Long id, Employee employee) {
        Employee targetEmployee = findById(id);
        targetEmployee.setAge(employee.getAge());
        targetEmployee.setSalary(employee.getSalary());
        return  targetEmployee;

    }


    public void deleteById(Long id) {
        employees.removeIf((employee -> Objects.equals(employee.getId(), id)));
    }

    public List<Employee> findByPage(int pageNumber, int pageSize) {
        return employees.stream()
                .skip((long) (pageNumber - 1) * pageSize)
                .limit(pageSize)
                .toList();

    }

    public List<Employee> findEmployeesByCompanyId(Long companyId){
        return employees.stream()
                .filter((employee)-> employee.getCompanyId() == companyId)
                .toList();
    }



}
